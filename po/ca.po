# Bing automated translation of popt messages
# This file is put in the public domain.
# bing-ca <popt-devel@rpm5.org>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: popt 1.17\n"
"Report-Msgid-Bugs-To: <popt-devel@rpm5.org>\n"
"POT-Creation-Date: 2017-03-19 12:46-0400\n"
"PO-Revision-Date: 2015-05-12 17:15-0400\n"
"Last-Translator: bing-ca <popt-devel@rpm5.org>\n"
"Language-Team: bing-ca <popt-devel@rpm5.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: popt.c:54
msgid "unknown errno"
msgstr "desconegut errno"

#: popt.c:1453
#, fuzzy, c-format
msgid "option type (%u) not implemented in popt"
msgstr "tipus d'opció (%u) no implementades en popt\n"

#: popt.c:1902
msgid "missing argument"
msgstr "l'argument que falten"

#: popt.c:1904
msgid "unknown option"
msgstr "opció desconeguda"

#: popt.c:1906
msgid "mutually exclusive logical operations requested"
msgstr "mútuament excloents operacions lògiques sol·licitat"

#: popt.c:1908
msgid "opt->arg should not be NULL"
msgstr "optar-> cat no hauria de ser nul"

#: popt.c:1910
msgid "aliases nested too deeply"
msgstr "àlies massa incrustats"

#: popt.c:1912
msgid "error in parameter quoting"
msgstr "error al paràmetre citant"

#: popt.c:1914
msgid "invalid numeric value"
msgstr "valor numèric no és vàlid"

#: popt.c:1916
msgid "number too large or too small"
msgstr "Número massa gran o massa petit"

#: popt.c:1918
msgid "memory allocation failed"
msgstr "assignació de memòria ha fallat"

#: popt.c:1920
msgid "config file failed sanity test"
msgstr "prova de seny fallit de fitxer config"

#: popt.c:1922
msgid "option does not take an argument"
msgstr "opció no pren un argument"

#: popt.c:1924
msgid "stack underflow"
msgstr "sotaeiximent de la pila"

#: popt.c:1926
msgid "stack overflow"
msgstr "Sobreeiximent de piles"

#: popt.c:1930
msgid "unknown error"
msgstr "error desconegut"

#: popthelp.c:84 popthelp.c:95
msgid "Show this help message"
msgstr "Mostra aquest missatge d'ajuda"

#: popthelp.c:85 popthelp.c:96
msgid "Display brief usage message"
msgstr "Missatge de pantalla breu d'ús"

#: popthelp.c:99
msgid "Display option defaults in message"
msgstr "Mostrar per defecte d'opció en el missatge"

#: popthelp.c:101
msgid "Terminate options"
msgstr "Opcions d'acabar"

#: popthelp.c:200
msgid "Help options:"
msgstr "Opcions d'ajuda:"

#: popthelp.c:201
msgid "Options implemented via popt alias/exec:"
msgstr "Opcions implementats Exec popt àlies /:"

#: popthelp.c:209
msgid "NONE"
msgstr "CAP"

#: popthelp.c:211
msgid "VAL"
msgstr "VAL"

#: popthelp.c:215
msgid "INT"
msgstr "INT"

#: popthelp.c:216
msgid "SHORT"
msgstr "CURT"

#: popthelp.c:217
msgid "LONG"
msgstr "LLARG"

#: popthelp.c:218
msgid "LONGLONG"
msgstr "LONGLONG"

#: popthelp.c:219
msgid "STRING"
msgstr "CORDA"

#: popthelp.c:220
msgid "FLOAT"
msgstr "SURAR"

#: popthelp.c:221
msgid "DOUBLE"
msgstr "DOBLE"

#: popthelp.c:224
msgid "ARG"
msgstr "CAT"

#: popthelp.c:665
msgid "Usage:"
msgstr "Ús:"

#: popthelp.c:690
msgid "[OPTION...]"
msgstr "[OPCIÓ...]"
